package uniandes.isis2304.epsandes.negocio;

import java.util.LinkedList;
import java.util.List;

public class ServicioSalud implements VOServicioSalud{

	private int capacidad;

	private String horarioAtencion;

	private long id;

	private String nombre;

	/**
	 * Las medicos de la IPS
	 * Cada objeto medico contiene [id, especialidad, numeRegMedico, tipo] 
	 */
	private List<Object []> medicos;


	/**
	 * Las consultas de la IPS
	 * Cada objeto consulta contiene [id, ordenPrevia, tipoConsulta] 
	 */
	private List<Object []> consultas;


	/**
	 * Constructor por defecto
	 */
	public ServicioSalud() {

		horarioAtencion = "";
		capacidad = 0;
		id = 0;
		nombre = "";
		medicos = new LinkedList<Object []> ();
		consultas = new LinkedList<Object []> ();

	}


	/**
	 * Constructor con valores
	 * @param id - El id del medico
	 * @param especialidad - La especialidad del medico
	 * @param numRegMedico - El numero de registro del medico
	 * @param tipo - El tipo de medico que es 
	 * @param nombre - El nombre del medico
	 */
	public ServicioSalud(long id, String horarioAtencion, int capacidad, String nombre) 
	{
		this.id = id;
		this.nombre = nombre;
		this.horarioAtencion = horarioAtencion;
		this.capacidad = capacidad;
		medicos = new LinkedList<Object []> ();
		consultas = new LinkedList<Object []> ();

	}

	@Override
	public int getCapacidad() {
		// TODO Auto-generated method stub
		return capacidad;
	}

	@Override
	public String getHorarioAtencion() {
		// TODO Auto-generated method stub
		return horarioAtencion;
	}

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return id;
	}


	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}

	public List<Object []> getMedicos() {

		return medicos;

	}

	public List<Object []> getConsultas() {

		return consultas;

	}

	@Override
	/**
	 * @return Una cadena de caracteres con todos los atributos del Servicio de salud
	 */
	public String toString() 
	{
		return "ServicioSalud [id= " + id + ", nombre= " + nombre + ", horarioAtencion= " + horarioAtencion + ", capacidad= " + capacidad + "]";
	}

}
