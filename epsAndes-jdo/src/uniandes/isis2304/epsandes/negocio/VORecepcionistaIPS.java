package uniandes.isis2304.epsandes.negocio;
/**
 * Interfaz para los métodos get del RecepcionistaIPS.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz 
 * 
 * @author Nicolas Potes Garcia - Jaime Torres
 * 
 * Este codigo esta basado en la aplicacion de parranderos realizada por German Bravo
 */
public interface VORecepcionistaIPS {
	
	
	/**
	 *@return nombre del recepcionista
	 */
	public String getNombre();
	
	
	/**
	 *@return id del recepcionista
	 */
	public long getId();
	
	
	@Override
	/**
	 * @return Una cadena de caracteres con todos los atributos del paciente
	 */
	public String toString();
	

}
