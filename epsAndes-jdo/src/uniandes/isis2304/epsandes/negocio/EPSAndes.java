package uniandes.isis2304.epsandes.negocio;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import com.google.gson.JsonObject;
//import uniandes.isis2304.epsandes.persistencia.PersistenciaEPSAndes;

import uniandes.isis2304.parranderos.negocio.TipoBebida;

/**
 * Clase principal del negocio
 * Satisface todos los requerimientos funcionales del negocio
 *
 * @author Nicolas Potes Garcia - Jaime Andres Torres
 * 
 * Este codigo esta basado en el proyecto de Parranderos realizado por German Bravo
 */

public class EPSAndes {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecuci�n
	 */
	private static Logger log = Logger.getLogger(EPSAndes.class.getName());

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	//private PersistenciaParranderos pp;


	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/
	/**
	 * El constructor por defecto
	 */
	public EPSAndes ()
	{
		pp = EPSAndesPersistencia.getInstance ();
	}

	/**
	 * El constructor qye recibe los nombres de las tablas en tableConfig
	 * @param tableConfig - Objeto Json con los nombres de las tablas y de la unidad de persistencia
	 */
	public EPSAndes (JsonObject tableConfig)
	{
		pp = PersistenciaParranderos.getInstance (tableConfig);
	}

	/**
	 * Cierra la conexi�n con la base de datos (Unidad de persistencia)
	 */
	public void cerrarUnidadPersistencia ()
	{
		pp.cerrarUnidadPersistencia ();
	}

	/* ****************************************************************
	 * 			M�todos para manejar los MEDICOS
	 *****************************************************************/


	/**
	 * Registra a la EPS de manera persistente un medico asociado a una o varias IPS
	 * Adiciona entradas al log de la aplicaci�n
	 */
	public Medico registrarMedico (String nombre, long id, String especialidad, int numRegMedico, String tipo, long idIPS)
	{	
		Medico medico;
		log.info ("Registrando medico: " + nombre + " con id: " + id);

		if(buscarMedicoPorId(id) == null) {

			medico = pp.registrarMedico (nombre, id, especialidad, numRegMedico, tipo, idIPS);		
			log.info ("Registrando medico: " + medico);


		} else {

			log.info("El medico no cumple con los requisitos para ser ingresado");

		}

		return medico;

	}


	/**
	 * Se busca un medico dentro de la lista de medicos existentes (por su id).
	 * @param id del medico a buscar
	 * @return medico o null por si no se encontro
	 */
	public Medico buscarMedicoPorId(long id) {

		Medico medicoId;
		log.info("Buscando medico");

		medicoId = pp.buscarMedicoPorId(id);
		log.info ("Medico encontrado: " + medicoId);

		return medicoId;

	}


	/* ****************************************************************
	 * 			M�todos para manejar las IPS
	 *****************************************************************/

	public IPS registrarIPS(long id, String nombre, String tipo, String localizacion, long idEPS) {



		IPS ips;
		log.info("Registrando IPS: " + nombre);

		ips = pp.registrarIPS(id, nombre, tipo, localizacion, idEPS);

		log.info ("IPS registrada: " + ips);

		return ips;

	}



	/* ****************************************************************
	 * 			M�todos para manejar los Afiliados (paciente)
	 *****************************************************************/

	public Paciente registrarAfiliado(long id, String nombre, String estado, long numDocumento, long tipoDocumento,
			String fechaNacimiento, long idEPS) {

		Paciente paciente;
		log.info("Registrando Paciente: " + nombre);

		paciente = pp.registrarPaciente(id, nombre, estado, numDocumento, tipoDocumento, fechaNacimiento, idEPS);

		log.info ("Paciente registrado: " + paciente);

		return paciente;


	}



	/* ****************************************************************
	 * 			M�todos para manejar los Servicios de salud
	 *****************************************************************/

	public ServicioSalud registrarServicioSalud(long id, int capacidad, String horarioAtencion,
			long idIPS, String nombre) {

			ServicioSalud servicio;
			log.info("Registrando Servicio de salud: " + nombre);

			servicio = pp.registrarServicioSalud(id, capacidad, horarioAtencion, idIPS, nombre);

			log.info ("Servicio de salud registrado: " + servicio);

			return servicio;

		}
	
	public void registrarOrdenServicioSalud(long idServicioSalud, long idMedico, long idAfiliado) {
		
		
		log.info("Registrando orden de servicio de salud");
		pp.registrarOrdenServicioSalud(idServicioSalud, idMedico, idAfiliado);
		log.info("Orden de servicio de salud registrado");
		
		
	}



	}
