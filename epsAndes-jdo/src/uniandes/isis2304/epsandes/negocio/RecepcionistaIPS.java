package uniandes.isis2304.epsandes.negocio;

import java.sql.Timestamp;

public class RecepcionistaIPS implements VORecepcionistaIPS{
	
	
	
	private String nombre;
	
	private long id;
	
	
	/**
	 * Constructor por defecto
	 */
	public RecepcionistaIPS() 
	{
		this.id = 0;
		this.nombre = "";
		
	}

	/**
	 * Constructor con valores
	 * @param nombre - El nombre del recepcionista de la IPS
	 * @param id - identificacion del recepcionista
	 */
	public RecepcionistaIPS(long id, String nombre) 
	{
		this.id = id;
		this.nombre = nombre;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	/** 
	 * @return Una cadena con la información básica
	 */
	@Override
	public String toString() 
	{
		return "RecepcionistaIPS [id=" + id + ", nombre=" + nombre + "]";
	}

}
